# Entaksi.eCon.LUL.Api.PdvApi

All URIs are relative to *https://test.entaksi.eu/lul/api/v1* (test server) or *https://entaksi.eu/lul/api/v1* (production server)

Method | HTTP request | Description
------------- | ------------- | -------------
[**Create**](PdvApi.md#create) | **POST** /pdv | Crea un nuovo pacchetto di versamento contenente il LUL caricando il file ZIP con il contenuto
[**Read**](PdvApi.md#read) | **GET** /pdv/{idPdv} | Recupera le informazioni di un pacchetto di versamento


<a name="create"></a>
# **Create**
> Pdv Create (System.IO.Stream pdv)

Crea un nuovo pacchetto di versamento contenente il LUL caricando il file ZIP con il contenuto

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.LUL.Api;
using Entaksi.eCon.LUL.Client;
using Entaksi.eCon.LUL.Model;

namespace Example
{
    public class CreateExample
    {
        public void main()
        {
            var apiInstance = new PdvApi();
            var pdv = new System.IO.Stream(); // System.IO.Stream | File ZIP con il contenuto del pacchetto di versamento

            try
            {
                // Crea un nuovo pacchetto di versamento contenente il LUL caricando il file ZIP con il contenuto
                Pdv result = apiInstance.Create(pdv);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdvApi.Create: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pdv** | **System.IO.Stream**| File ZIP con il contenuto del pacchetto di versamento | 

### Return type

[**Pdv**](Pdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read"></a>
# **Read**
> Pdv Read (long? idPdv)

Recupera le informazioni di un pacchetto di versamento

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.LUL.Api;
using Entaksi.eCon.LUL.Client;
using Entaksi.eCon.LUL.Model;

namespace Example
{
    public class ReadExample
    {
        public void main()
        {
            var apiInstance = new PdvApi();
            var idPdv = 789;  // long? | Identificativo del pacchetto di versamento

            try
            {
                // Recupera le informazioni di un pacchetto di versamento
                Pdv result = apiInstance.Read(idPdv);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdvApi.Read: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdv** | **long?**| Identificativo del pacchetto di versamento | 

### Return type

[**Pdv**](Pdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

