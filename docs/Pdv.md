# Entaksi.eCon.LUL.Model.Pdv
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**Stato** | **string** | Stato del pacchetto di versamento | [optional] 
**NomeFile** | **string** | Nome del file del pacchetto di versamento | [optional] 
**EconId** | **long?** | Identificativo del pacchetto di versamento nel sistema di conservazione | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

