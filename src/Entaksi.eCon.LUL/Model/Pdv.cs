/* 
 * Entaksi eDoc LUL
 *
 * API REST per l'accesso ai servizi di gestione ed archiviazione del Libro Unico del Lavoro.
 *
 * OpenAPI spec version: 1.1.11-SNAPSHOT
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = Entaksi.eCon.LUL.Client.SwaggerDateConverter;

namespace Entaksi.eCon.LUL.Model
{
    /// <summary>
    /// Pacchetto di versamento con cui vengono caricati i contenuti da portare in conservazione elettronica
    /// </summary>
    [DataContract]
    public partial class Pdv :  IEquatable<Pdv>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pdv" /> class.
        /// </summary>
        /// <param name="id">Identificativo dell&#39;entità.</param>
        /// <param name="stato">Stato del pacchetto di versamento.</param>
        /// <param name="nomeFile">Nome del file del pacchetto di versamento.</param>
        /// <param name="econId">Identificativo del pacchetto di versamento nel sistema di conservazione.</param>
        public Pdv(long? id = default(long?), string stato = default(string), string nomeFile = default(string), long? econId = default(long?))
        {
            this.Id = id;
            this.Stato = stato;
            this.NomeFile = nomeFile;
            this.EconId = econId;
        }
        
        /// <summary>
        /// Identificativo dell&#39;entità
        /// </summary>
        /// <value>Identificativo dell&#39;entità</value>
        [DataMember(Name="id", EmitDefaultValue=false)]
        public long? Id { get; set; }

        /// <summary>
        /// Stato del pacchetto di versamento
        /// </summary>
        /// <value>Stato del pacchetto di versamento</value>
        [DataMember(Name="stato", EmitDefaultValue=false)]
        public string Stato { get; set; }

        /// <summary>
        /// Nome del file del pacchetto di versamento
        /// </summary>
        /// <value>Nome del file del pacchetto di versamento</value>
        [DataMember(Name="nome_file", EmitDefaultValue=false)]
        public string NomeFile { get; set; }

        /// <summary>
        /// Identificativo del pacchetto di versamento nel sistema di conservazione
        /// </summary>
        /// <value>Identificativo del pacchetto di versamento nel sistema di conservazione</value>
        [DataMember(Name="econ_id", EmitDefaultValue=false)]
        public long? EconId { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Pdv {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Stato: ").Append(Stato).Append("\n");
            sb.Append("  NomeFile: ").Append(NomeFile).Append("\n");
            sb.Append("  EconId: ").Append(EconId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Pdv);
        }

        /// <summary>
        /// Returns true if Pdv instances are equal
        /// </summary>
        /// <param name="input">Instance of Pdv to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Pdv input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Id == input.Id ||
                    (this.Id != null &&
                    this.Id.Equals(input.Id))
                ) && 
                (
                    this.Stato == input.Stato ||
                    (this.Stato != null &&
                    this.Stato.Equals(input.Stato))
                ) && 
                (
                    this.NomeFile == input.NomeFile ||
                    (this.NomeFile != null &&
                    this.NomeFile.Equals(input.NomeFile))
                ) && 
                (
                    this.EconId == input.EconId ||
                    (this.EconId != null &&
                    this.EconId.Equals(input.EconId))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Id != null)
                    hashCode = hashCode * 59 + this.Id.GetHashCode();
                if (this.Stato != null)
                    hashCode = hashCode * 59 + this.Stato.GetHashCode();
                if (this.NomeFile != null)
                    hashCode = hashCode * 59 + this.NomeFile.GetHashCode();
                if (this.EconId != null)
                    hashCode = hashCode * 59 + this.EconId.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
