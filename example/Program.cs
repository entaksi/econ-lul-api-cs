﻿using System;
using System.IO;
using Entaksi.eCon.LUL.Api;
using Entaksi.eCon.LUL.Client;
using Entaksi.eCon.LUL.Model;

namespace EconLULSampleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                /* 
                 * Configurazione programmatica del client, da utilizzare per istanziare le classi `*Api.cs`.
                 * In alternativa, la configurazione può essere specificata come default globale 
                 * nella classe `Client/GlobalConfiguration.cs`.
                 */
                Configuration sampleConfiguration = new Configuration
                {
                    /* Url del server di test. 
                     * Il server di produzione risponde a https://entaksi.eu/lul/api/v1 */
                    BasePath = "https://test.entaksi.eu/lul/api/v1",
                    Username = "username",
                    Password = "password",
                    ClientID = "client-id",
                    Secret = "secret"
                };

                PdvApi pdvApi = new PdvApi(sampleConfiguration);

                /* Caricamento del file ZIP con il contenuto del pacchetto di versamento */
                FileStream filePdv = File.OpenRead(@"c\pacchetto.zip");

                /* Creazione del pacchetto di versamento */
                Pdv pdvCreato = pdvApi.Create(filePdv);

                Console.WriteLine("PDV '" + pdvCreato.EconId + "' creato");

                /* Recupero del pacchetto di versamento tramite identificativo */
                Pdv pdv = pdvApi.Read(pdvCreato.Id);

                Console.WriteLine("PDV '" + pdv.NomeFile + "' caricato (stato: '" + pdv.Stato + "')");

            }
            catch (Entaksi.eCon.LUL.Client.ApiException ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
    }
}
